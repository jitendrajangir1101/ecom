from django.contrib import admin
from .models import Product ,Customer ,Cart ,Orderplaced

@admin.register(Customer)
class customerModeladmin(admin.ModelAdmin):
    list_display = ['id' ,'user' ,'locality' ,'city' ,'state' ,'zipcode']

@admin.register(Product)
class productModeladmin(admin.ModelAdmin):
    list_display = ['id' ,'title' ,'selling_price' ,'discounted_price' ,'discription' ,'brand' ,'category' ,'product_image']

@admin.register(Cart)
class cartModeladmin(admin.ModelAdmin):
    list_display = ['id' ,'user' ,'product' ,'quantity']

@admin.register(Orderplaced)
class orderModeladmin(admin.ModelAdmin):
    list_display = ['id' ,'user' ,'customer' ,'product' ,'quantity' ,'order_date' ,'status']
# Register your models here.
